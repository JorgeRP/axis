const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const webpack = require('webpack');
const conf = require('./conf/local.js');

module.exports = {
    plugins: [
        new ProgressBarPlugin()
    ],
    devtool: 'inline-source-map',
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(conf.NODE_ENV),
                ORIGIN: conf.ORIGIN
            },
        }),
        new ProgressBarPlugin(),
    ],
    module: {
        rules: [{
            test: /.js$/,
            exclude: [/node_modules(?!\/webpack-dev-server)/, /\.spec\.js$/, /test.bundle\.js$/],
            use: [{
                loader: 'babel-loader',
                options: {
                    presets: ['env']
                }
            }, {
                loader: 'istanbul-instrumenter-loader',
                query: {
                    esModules: true
                }
            }]
        }
        ]
    }
};