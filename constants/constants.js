export const COOKIES = {
  KEY: "cookies",
  POST_EVENT: "sct-cookies",
  DELETE_EVENT: "sct-cookies-delete",
  EXPIRE_KEY: "=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;",
  PATH: "path=/",
  SECURE: ";secure",
  NO_COOKIES_KEY: "no_sct_cookie",
  LISTENER: {
    MESSAGE_KEY: "message"
  }
};

export const STORAGE_KEYS = ["Lsacstt", "ascccte", "isdcTto", "rsecftr"];
