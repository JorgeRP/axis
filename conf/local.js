module.exports = {
  NODE_ENV: "develop",
  ORIGIN: JSON.stringify([
    {
      origin: "localhost:4200",
      allow: ["get", "set", "del", "getKeys", "clear"]
    },
    {
      origin: "localhost:8081",
      allow: ["get", "set", "del", "getKeys", "clear"]
    }
  ])
};
