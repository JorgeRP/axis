import * as CONSTANTS from "../../../constants/constants";

const cookies = {
  key: CONSTANTS.COOKIES.KEY,
  value: document.cookie
};

export default class CookieService {
  static sendCookies(cookie) {
    if (
      document.cookie.length > 0 &&
      this.hasSctCookies(this.formatCookies(cookie))
    ) {
      window.parent.postMessage(cookies, "*");
    } else if (
      (document.cookie.length > 0 &&
        !this.hasSctCookies(this.formatCookies(cookie))) ||
      document.cookie.length <= 0
    ) {
      window.parent.postMessage(
        {
          key: CONSTANTS.COOKIES.NO_COOKIES_KEY
        },
        "*"
      );
    }
  }

  static formatCookies(cookieData) {
    cookieData = cookieData
      .replace(/;/g, ",")
      .replace(/\s/g, "")
      .replace(/=/g, ":")
      .split(",");
    for (let i = 0; i < cookieData.length; i += 1) {
      cookieData[i] = cookieData[i].split(":");
    }
    const obj = {};
    cookieData.forEach(data => {
      obj[data[0]] = data[1];
    });

    return obj;
  }

  static setCookies(cookieData, expirationDate) {
    Object.keys(cookieData).forEach(key => {
      if (CONSTANTS.STORAGE_KEYS.includes(key)) {
        document.cookie = `${key}=${cookieData[key]};${
          CONSTANTS.COOKIES.PATH
        };expires=${expirationDate}${CONSTANTS.COOKIES.SECURE};domain=${
          document.location.host
        }`;
      }
    });
  }

  static deleteCookies(cookieData) {
    Object.keys(cookieData).forEach(key => {
      if (CONSTANTS.STORAGE_KEYS.includes(key)) {
        document.cookie = `${key}=${cookieData[key]}${
          CONSTANTS.COOKIES.EXPIRE_KEY
        }domain=${document.location.host};`;
      }
    });
  }

  static hasSctCookies(cookieData) {
    let array = [];
    Object.keys(cookieData).forEach(key => {
      if (CONSTANTS.STORAGE_KEYS.includes(key)) {
        array.push(key);
      }
    });

    if (array.length > 0) {
      return true;
    } else {
      return false;
    }
  }
}
