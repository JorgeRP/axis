import CookieService from './cookie.service';

describe('CookieService', () => {
  it('should send message', () => {
    document.cookie = 'Lsacstt=target;';
    spyOn(window.parent, 'postMessage');
    CookieService.sendCookies(document.cookie);
    expect(window.parent.postMessage).toHaveBeenCalled();
    // delete cookies
    CookieService.deleteCookies(CookieService.formatCookies(document.cookie));
  });

  it('should delete cookies', () => {
    const cookies = {
      Lsacstt: 'target',
    };
    CookieService.deleteCookies(cookies);
    expect(document.cookie).toEqual('');
  });
});
