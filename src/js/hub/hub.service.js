import * as CONSTANTS from "../../../constants/constants";
import CookieService from "../shared/cookie.service";

class HubService {
  static receiveMessage(event) {
    if (
      (event.data.key === CONSTANTS.COOKIES.POST_EVENT ||
        event.data.key === CONSTANTS.COOKIES.DELETE_EVENT) &&
      event.data.value.length > 0
    ) {
      const cookies = CookieService.formatCookies(event.data.value);
      if (event.data.key === CONSTANTS.COOKIES.POST_EVENT) {
        CookieService.setCookies(cookies, event.data.expirationDate);
      } else if (event.data.key === CONSTANTS.COOKIES.DELETE_EVENT) {
        CookieService.deleteCookies(cookies);
      }
    }
  }
}

export default HubService;
