
import CrossStorage from 'cross-storage';
import * as CONSTANTS from '../../../constants/constants';
import HubService from './hub.service';
import CookieService from '../shared/cookie.service';

export default class HubController {

  init() {
    window.addEventListener(
      CONSTANTS.COOKIES.LISTENER.MESSAGE_KEY,
      HubService.receiveMessage,
      true,
    );
    const urls = process.env.ORIGIN;
    urls.forEach((url) => {
      url.origin = new RegExp(url.origin);
    });
    CrossStorage.CrossStorageHub.init(urls);
    CookieService.sendCookies(document.cookie);
  }
}
