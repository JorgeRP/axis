import HubService from './hub.service';
import CookieService from '../shared/cookie.service';

describe('HubService', () => {
  it('should call setCookies when sct-cookies event', () => {
    const event = {
      data: {
        key: 'sct-cookies',
        value: 'Lsacstt=test',
      },
    };
    spyOn(CookieService, 'setCookies');
    HubService.receiveMessage(event);
    expect(CookieService.setCookies).toHaveBeenCalled();
  });

  it('should call setCookies when sct-cookies event', () => {
    const event = {
      data: {
        key: 'sct-cookies-delete',
        value: 'Lsacstt=test',
      },
    };
    spyOn(CookieService, 'deleteCookies');
    HubService.receiveMessage(event);
    expect(CookieService.deleteCookies).toHaveBeenCalled();
  });
});
