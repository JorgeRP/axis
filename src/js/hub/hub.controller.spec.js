import HubController from './hub.controller';
import CookieService from '../shared/cookie.service';
// meter tests en proyecto
describe('HubController', () => {
  it('should call sendCookies and addEventListener', () => {
    const hub = new HubController();
    spyOn(CookieService, 'sendCookies');
    spyOn(window, 'addEventListener');
    hub.init();
    expect(CookieService.sendCookies).toHaveBeenCalled();
    expect(window.addEventListener).toHaveBeenCalled();
  });
});
