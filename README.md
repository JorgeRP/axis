# SCT UI HUB

1. Requeriments
2. Instalation
3. Execution
4. Build

## Requeriments

Node version >= 6
Npm version >= 5 

## Instalation

Execute npm install for dependencies

Inside conf you can put an array of stringified objects with information about the urls to connect.This project uses
CrossStorage library by Zendesk. 

## Execution

Run npm run env-* where * can be:

- local
- dev
- test
- pro


## Build

Run npm run bundle-* where * can be:S

- local
- dev
- test
- pro
